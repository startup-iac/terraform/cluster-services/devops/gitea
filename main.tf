# For more info view:
# https://artifacthub.io/packages/helm/gitea/gitea

locals {
  secret_name   = "${var.dns_name}-tls"
  values_file   = "values-${var.chart_version}.yaml"
  serverBaseURL = "https://${var.dns_name}"
}

data "template_file" "values" {
  template = "${file("${path.module}/${local.values_file}")}"
  vars = {
    DNS_NAME          = var.dns_name
    IAM_DNS_NAME      = var.iam_dns_name
    IAM_CLIENT_SECRET = var.iam_client_secret
  }
}

resource "helm_release" "gitea" {
  name             = "gitea"
  namespace        = var.namespace

  repository = "https://dl.gitea.com/charts/"
  chart      = "gitea"

  values = [    
    data.template_file.values.rendered
  ]

  set {
    name  = "ingress.hosts[0].host"
    value = var.dns_name
  }

  set {
    name  = "ingress.tls[0].secretName"
    value = local.secret_name
  }

  set {
    name  = "ingress.tls[0].hosts[0]"
    value = var.dns_name
  }

  # Ingress Annotations
  set {
    name  = "ingress.annotations.cert-manager\\.io/cluster-issuer"
    value = var.cluster_issuer_name
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/common-name"
    value = var.dns_name
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/subject-organizations"
    value = var.subject_organizations
  }  

  set {
    name  = "ingress.annotations.cert-manager\\.io/subject-organizationalunits"
    value = var.subject_organizationalunits
  }
}

